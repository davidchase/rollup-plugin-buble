# rollup-plugin-buble changelog

## 0.12.1

* Return a `name`

## 0.12.0

* Update `buble`

## 0.11.0

* Update `buble`

## 0.10.0

* Update `buble`

## 0.9.0

* Update `buble`

## 0.8.0

* Update `buble`

## 0.7.0

* Update `buble`

## 0.6.0

* Update `buble`

## 0.5.0

* Update `buble`

## 0.4.0

* Update `buble`

## 0.3.4

* Update `buble`

## 0.2.3

* Make plugin 0.12-friendly

## 0.2.2

* Update `buble`

## 0.1.0

* First release
